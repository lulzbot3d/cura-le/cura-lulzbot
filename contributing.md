Submitting bug reports
----------------------
Please submit bug reports for all of Cura LulzBot Edition and CuraEngineLE to the [Cura LulzBot Edition repository](https://gitlab.com/lulzbot3d/cura-le/cura-lulzbot/-/issues). Depending on the type of issue, we will usually ask for the CuraLE log or a project file.

If a bug report would contain private information, such as a proprietary 3D model, you may also e-mail us. Ask for contact information in the issue.

Requesting features
-------------------
When requesting a feature, please describe clearly what you need and why you think this is valuable to users or what problem it solves. Please make sure that feature requests are relevant to CuraLE as an application, hardware requests for LulzBot printers are better handled by our support team.
