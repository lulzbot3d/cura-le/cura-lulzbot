<!--
Before creating a new issue, please use the search bar on the issues page to see if someone has already made one. If you find one (open or closed), add your comments there.

Give your issue a clear and short title. Avoid general words like "BUG" or "SUGGESTION".

For each section, write your response on the blank space directly below each header line.

Thank you for helping to improve Cura LulzBot Edition!
-->

**Cura LE Version** <!--e.g. 3.6.37, 4.13.2-->



**Platform** <!--Windows, MacOS, or Linux-->



**Printer and Tool Head** <!--Which printer/tool head combination was selected in Cura LE?-->



**Description of Issue** <!--Describe what problem you are encountering.-->



**Screenshot(s)** <!--Image showing the problem, if relevant-->



**Log or other relevant file** <!--Logs are very valuable for many non-UI bugs, we'd appreciate one-->



<!--
Logs can be found in the following locations:
Windows: C:\Users\[Your Username]\AppData\Roaming\cura-le\4.13\cura-le.log
Linux:   /home/[Your Username]/.local/share/cura-le/4.13/cura-le.log
MacOS:   [TBD]
-->

